import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import {NewMovies} from './newMovies/newMovies';
import {Movie} from './movie/movie';
import {withSearch} from './form/form'
import './index.css';

class App extends React.Component {
  render() {
    return(
      <BrowserRouter>
        <Switch>
          <Route path={'/movie/:id'} component={withSearch(Movie)} />
          <Route path={'/'} component={withSearch(NewMovies)} />
        </Switch>
      </BrowserRouter>
    );
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
