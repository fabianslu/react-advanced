import React from 'react';
import {Link} from 'react-router-dom';
import './formResults.css';

export const FormResults = (props) => {
  const handleClick = () => {
    document.getElementById('results').className = 'noDisplay';
    document.getElementById('searchInput').value = '';
  }
  const link = 'https://image.tmdb.org/t/p/w300';
  return (
    <ul id="results" onClick={handleClick}>
      {props.results.map((element, index) => {
        return(
          <li key={index} onClick={handleClick}>
            <Link to={`/movie/${props.results[index].id}`} >
              <img src={props.results[index].poster_path === null ? 'http://via.placeholder.com/300x450' : `${link}${props.results[index].poster_path}`} alt={`${props.results[index].title} poster`} className="resultPoster" />
              <div>
                <p>{props.results[index].title}</p>
                <p>{props.results[index].release_date}</p>
              </div>
            </Link>
          </li>
        )
      })}
    </ul>
  );
}
